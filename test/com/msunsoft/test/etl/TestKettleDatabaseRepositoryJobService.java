package com.msunsoft.test.etl;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.msunsoft.main.etl.entity.R_JOB;
import com.msunsoft.main.etl.service.KettleJobService;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:resources/spring/applicationContext.xml")
public class TestKettleDatabaseRepositoryJobService {

	@Autowired
	KettleJobService kettleJobService;
	
	@Test
	public void getJobALL(){
		List<R_JOB> jobList = kettleJobService.getJobALL();
		for(R_JOB job:jobList)
		System.out.println(job.getName()+" path=> "+job.getAbsolutePath());
	}
	
}
