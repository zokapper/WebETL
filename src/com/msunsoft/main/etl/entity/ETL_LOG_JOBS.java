package com.msunsoft.main.etl.entity;

import java.math.BigDecimal;
import java.util.Date;

public class ETL_LOG_JOBS {
    private BigDecimal idJob;

    private String channelId;

    private String jobname;

    private String status;

    private BigDecimal linesRead;

    private BigDecimal linesWritten;

    private BigDecimal linesUpdated;

    private BigDecimal linesInput;

    private BigDecimal linesOutput;

    private BigDecimal linesRejected;

    private BigDecimal errors;

    private Date startdate;

    private Date enddate;

    private String logdate;

    private Date depdate;

    private Date replaydate;

    private String executingServer;

    private String executingUser;

    private String startJobEntry;

    private String client;

    private String logField;

    public BigDecimal getIdJob() {
        return idJob;
    }

    public void setIdJob(BigDecimal idJob) {
        this.idJob = idJob;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId == null ? null : channelId.trim();
    }

    public String getJobname() {
        return jobname;
    }

    public void setJobname(String jobname) {
        this.jobname = jobname == null ? null : jobname.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public BigDecimal getLinesRead() {
        return linesRead;
    }

    public void setLinesRead(BigDecimal linesRead) {
        this.linesRead = linesRead;
    }

    public BigDecimal getLinesWritten() {
        return linesWritten;
    }

    public void setLinesWritten(BigDecimal linesWritten) {
        this.linesWritten = linesWritten;
    }

    public BigDecimal getLinesUpdated() {
        return linesUpdated;
    }

    public void setLinesUpdated(BigDecimal linesUpdated) {
        this.linesUpdated = linesUpdated;
    }

    public BigDecimal getLinesInput() {
        return linesInput;
    }

    public void setLinesInput(BigDecimal linesInput) {
        this.linesInput = linesInput;
    }

    public BigDecimal getLinesOutput() {
        return linesOutput;
    }

    public void setLinesOutput(BigDecimal linesOutput) {
        this.linesOutput = linesOutput;
    }

    public BigDecimal getLinesRejected() {
        return linesRejected;
    }

    public void setLinesRejected(BigDecimal linesRejected) {
        this.linesRejected = linesRejected;
    }

    public BigDecimal getErrors() {
        return errors;
    }

    public void setErrors(BigDecimal errors) {
        this.errors = errors;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public String getLogdate() {
        return logdate;
    }

    public void setLogdate(String logdate) {
        this.logdate = logdate;
    }

    public Date getDepdate() {
        return depdate;
    }

    public void setDepdate(Date depdate) {
        this.depdate = depdate;
    }

    public Date getReplaydate() {
        return replaydate;
    }

    public void setReplaydate(Date replaydate) {
        this.replaydate = replaydate;
    }

    public String getExecutingServer() {
        return executingServer;
    }

    public void setExecutingServer(String executingServer) {
        this.executingServer = executingServer == null ? null : executingServer.trim();
    }

    public String getExecutingUser() {
        return executingUser;
    }

    public void setExecutingUser(String executingUser) {
        this.executingUser = executingUser == null ? null : executingUser.trim();
    }

    public String getStartJobEntry() {
        return startJobEntry;
    }

    public void setStartJobEntry(String startJobEntry) {
        this.startJobEntry = startJobEntry == null ? null : startJobEntry.trim();
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client == null ? null : client.trim();
    }

    public String getLogField() {
        return logField;
    }

    public void setLogField(String logField) {
        this.logField = logField == null ? null : logField.trim();
    }

	@Override
	public String toString() {
		return "ETL_LOG_JOBS [idJob=" + idJob + ", channelId=" + channelId + ", jobname=" + jobname + ", status="
				+ status + ", linesRead=" + linesRead + ", linesWritten=" + linesWritten + ", linesUpdated="
				+ linesUpdated + ", linesInput=" + linesInput + ", linesOutput=" + linesOutput + ", linesRejected="
				+ linesRejected + ", errors=" + errors + ", startdate=" + startdate + ", enddate=" + enddate
				+ ", logdate=" + logdate + ", depdate=" + depdate + ", replaydate=" + replaydate + ", executingServer="
				+ executingServer + ", executingUser=" + executingUser + ", startJobEntry=" + startJobEntry
				+ ", client=" + client + ", logField=" + logField + "]";
	}
    
    
}