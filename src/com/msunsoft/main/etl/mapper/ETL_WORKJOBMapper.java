package com.msunsoft.main.etl.mapper;

import java.util.List;
import java.util.Map;

import com.msunsoft.main.etl.entity.ETL_WORKJOB;

public interface ETL_WORKJOBMapper {
    int insertSelective(Map job);
    List<ETL_WORKJOB> selectETLWorkingJobs(Map job);
	List<ETL_WORKJOB> selectETLAllWorkingJobs();
}