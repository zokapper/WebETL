package com.msunsoft.main.etl.mapper;

import java.util.List;
import java.util.Map;

import com.msunsoft.main.etl.entity.ETL_LOG_JOB_METRIC;

public interface ETL_LOG_JOB_METRICMapper {

    /**
	 * 查询作业度量信息
	 * @param etlLogJobs
	 * @return
	 */
	List<ETL_LOG_JOB_METRIC> selectETLJobMetrics(Map etlLogJobs);
}