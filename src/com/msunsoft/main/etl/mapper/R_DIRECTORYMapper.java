package com.msunsoft.main.etl.mapper;

import com.msunsoft.main.etl.entity.R_DIRECTORY;
import java.math.BigDecimal;

public interface R_DIRECTORYMapper {
    int deleteByPrimaryKey(int idDirectory);

    int insert(R_DIRECTORY record);

    int insertSelective(R_DIRECTORY record);

    

    int updateByPrimaryKeySelective(R_DIRECTORY record);

    int updateByPrimaryKey(R_DIRECTORY record);
    
    /**
     * 查询指定目录Id的目录信息
     */
    
    R_DIRECTORY selectByPrimaryKey(int idDirectory);
}