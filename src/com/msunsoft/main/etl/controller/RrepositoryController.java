package com.msunsoft.main.etl.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/repostiry")
public class RrepositoryController {
	
	@RequestMapping(value="/repositoryBrowserView")
	public ModelAndView repositoryBrowserView(){
		ModelAndView view = new ModelAndView();
		view.setViewName("repository/browserView");
		return view;
	}
}
