package com.msunsoft.main.etl.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.msunsoft.main.etl.engine.KettleEnvironmentRunEngineService;
import com.msunsoft.main.etl.entity.ETL_WORKJOB;
import com.msunsoft.main.etl.entity.R_JOB;
import com.msunsoft.main.etl.service.KettleJobService;
import com.msunsoft.main.etl.service.ScriptSchedulerService;

/**
 * 作业运行控制
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value="/etlSchedule")
public class ScriptScheduleController {
	
	@Resource
	private KettleJobService kettleJobService;
	
	@Resource
	private ScriptSchedulerService scriptSchedulerService;
	
	@Resource
	private KettleEnvironmentRunEngineService runJobContainer;
	
	@RequestMapping(value="/scheduleControllerView")
	public ModelAndView scheduleControllerView(){
		ModelAndView view = new ModelAndView();
		view.setViewName("etl-schedule/scheduleController");
		return view;
	}
	
	@RequestMapping(value="/addJobScheduleView")
	public ModelAndView addJobScheduleView(){
		ModelAndView view = new ModelAndView();
		view.setViewName("etl-schedule/scheduleAddJob");
		return view;
	}
	
	@RequestMapping(value="/loadRepositoryJobs")
	public @ResponseBody String loadRepositoryJobs(){
		List<R_JOB> jobList = kettleJobService.getJobALL();
		Map reponse = new HashMap();
		reponse.put("Rows", jobList);
		ObjectMapper objectWraper = new ObjectMapper();
		try {
			return objectWraper.writeValueAsString(reponse);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value="/addRepositoryJobs",method = {RequestMethod.POST} , produces = { "text/json;charset=utf-8" })
	public void addRepositoryJobs(@RequestBody Map job){
		scriptSchedulerService.saveWorkJob(job);
	}
	
	/**
	 * 加载指定运行作业
	 */
	@RequestMapping(value="loadWorkingJobs",method={RequestMethod.POST},produces={"text/json;charset=utf-8"})
	public @ResponseBody String loadWorkingJobs(){
		System.out.println();
		List<ETL_WORKJOB> workingJobsList = scriptSchedulerService.selectAllWorkJob();
		Map reponse = new HashMap();
		reponse.put("Rows", workingJobsList);
		ObjectMapper objMapper = new ObjectMapper();
		try {
			return objMapper.writeValueAsString(reponse);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 启动指定运行作业
	 */
	@RequestMapping(value="startJobformation",produces={"text/json;charset=utf-8"})
	public @ResponseBody String startupWorkingJobs(@RequestBody List<Map> workJobsList){
		List result = new LinkedList();
		for(Map workJob:workJobsList){
			runJobContainer.runJobformation(new Long(workJob.get("idJob").toString()));
			String jobStatus = "正在运行";
			workJob.put("jobStatus", jobStatus);
			Map resultStart = new HashMap();
			resultStart.put("code", "200");
			resultStart.put("msg", "正在运行");
			resultStart.put("data",workJob);
			result.add(resultStart);
		}
		ObjectMapper objMapper = new ObjectMapper();
		try {
			return objMapper.writeValueAsString(result);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "success";
	}
	
	
	/**
	 * 启动指定运行作业
	 */
	@RequestMapping(value="stopJobformation",produces={"text/json;charset=utf-8"})
	public @ResponseBody String stopJobformation(@RequestBody List<Map> workJobsList){
		for(Map workJob:workJobsList)
		runJobContainer.stopJobformation(new Long(workJob.get("idJob").toString()));
		return null;
	}
	
	/**
	 * 获取所有作业的运行状态
	 */
	@RequestMapping(value="getJobformationStatus",produces={"text/json;charset=utf-8"})
	public @ResponseBody String getJobformationStatus(@RequestBody List<Map> workJobsList){
		List result = new LinkedList();
		for(Map workJob:workJobsList){
			String jobStatus = runJobContainer.getJobStatusById(new Long(workJob.get("idJob").toString()));
			workJob.put("jobStatus", jobStatus);
			Map resultStart = new HashMap();
			resultStart.put("code", "200");
			resultStart.put("msg", jobStatus);
			resultStart.put("data",workJob);
			result.add(resultStart);
		}
		ObjectMapper objMapper = new ObjectMapper();
		try {
			return objMapper.writeValueAsString(result);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "success";
	}
}

