package com.msunsoft.main.etl.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.msunsoft.main.etl.entity.R_DIRECTORY;
import com.msunsoft.main.etl.mapper.R_DIRECTORYMapper;
import com.msunsoft.main.etl.mapper.R_JOBMapper;

@Service
@Transactional
public class KettleDirectoryService {

	@Resource
	private R_JOBMapper jobMapper;
	
	@Resource 
	private R_DIRECTORYMapper directoryMapper;

	/**
	 * 获取资源指定目录id的完整路径
	 */
	
	public String getAbsolutePath(int rDirectoryId) {
		List<String> absolutePath = new ArrayList<String>();
		diguiQueryAbsolutePath(rDirectoryId,absolutePath);
		StringBuilder strBuilder = new StringBuilder();
		for(int i=absolutePath.size()-1;i>=0;i--){
			if(i==absolutePath.size()-1)
				strBuilder.append("/");
			strBuilder.append(absolutePath.get(i)).append("/");
		}
		return strBuilder.toString();
	}
	
	private void diguiQueryAbsolutePath(int rDirectoryId,List<String> absolutePath){
		R_DIRECTORY dbKettleDirectory =  directoryMapper.selectByPrimaryKey(rDirectoryId);
		if (dbKettleDirectory != null) {
			if (dbKettleDirectory.getIdDirectoryParent() == 0) {
				absolutePath.add(dbKettleDirectory.getDirectoryName());
			} else {
				absolutePath.add(dbKettleDirectory.getDirectoryName());
				diguiQueryAbsolutePath(dbKettleDirectory.getIdDirectoryParent(),absolutePath);
			}

		} 
	}
}
