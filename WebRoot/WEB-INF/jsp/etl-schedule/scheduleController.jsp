<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Kettle脚本运行控制台</title>
	    <link href="<%=request.getContextPath()%>/static/weblib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
	    <link href="<%=request.getContextPath()%>/static/weblib/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
	    <link href="<%=request.getContextPath()%>/static/weblib/ligerUI/skins/Gray/css/all.css" rel="stylesheet" type="text/css" />
	    <script src="<%=request.getContextPath()%>/static/weblib//jquery/jquery-1.10.2.min.js" type="text/javascript"></script> 
	    <script src="<%=request.getContextPath()%>/static/weblib/ligerUI/js/core/base.js" type="text/javascript"></script> 
	    <script src="<%=request.getContextPath()%>/static/weblib/ligerUI/js/plugins/ligerGrid.js" type="text/javascript"></script>
	    <script src="<%=request.getContextPath()%>/static/weblib/ligerUI/js/plugins/ligerToolBar.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/static/weblib/ligerUI/js/plugins/ligerResizable.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/static/weblib/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/static/weblib/layer1.8/layer.min.js" type="text/javascript"></script>
	        
	    <script type="text/javascript">
	    	var grid;
	        $(function ()
	        {
	        	grid = $("#maingrid").ligerGrid({
	                height:'100%',
	                columns: [
		                { display: '作业ID', name: 'idJob', align: 'left', width: 100, minWidth: 60 },
		                { display: '作业名称', name: 'name', minWidth: 120 },
		                { display: '作业路径', name: 'absolutePath', minWidth: 140 },
		                { display: '作业状态', name: 'jobStatus' }
	                ], 
	                url:'<%=request.getContextPath()%>/etlSchedule/loadWorkingJobs.action',
	                usePager:false,
	                checkbox: true,
	                onAfterShowData:refreshJobformationStatus,
	                toolbar: { 
	                	items: [
			                { text: '启动', click: startJobformation, icon: 'add' },
			                { line: true },
			                { text: '停止', click: stopJobformation, icon: 'modify' },
			                { line: true },
			                { text: '添加', click: addJobFormation, icon: 'add' },
			                { line: true },
			                { text: '删除', click: itemclick, icon: 'add' },
			                { line: true },
			                { text: '查看日志', click: openJobformationLogs, icon: 'add' },
			                { line: true },
			                { text: '刷新', click: refreshGrid, icon: 'add' }
		                ]
	                }
	            });
	             
	            $("#pageloading").hide();
	            
	        });
			function refreshGrid(){
				grid.options.url ='<%=request.getContextPath()%>/etlSchedule/loadWorkingJobs.action';
				grid.options.contentType = 'application/json;charset=UTF-8';
				grid.loadServerData(false);
			}
		    /*弹出层*/
		    function addJobFormation(){
		    	var url= '<%=request.getContextPath()%>/etlSchedule/addJobScheduleView.action';
		    	var w = 1000;
		    	var h = null;
		    	var title='添加新作业';
		    	open_dialog(w, h, title, url);
		    }
		    
		    /*弹出层*/
		    function openJobformationLogs(){
		    	var selected = grid.getSelectedRow();
		    	var url= '<%=request.getContextPath()%>/etlLogs/jobformationLogsView.action?jobName='+selected.name;
		    	var w = 1000;
		    	var h = null;
		    	var title='查看作业['+selected.name+']运行日志';
		    	open_dialog(w, h, title, url);
		    }
		    
		    function itemclick(item)
	        {
	            alert(item.text);
	        }
		    
		    function startJobformation(){
		    	var rows = grid.getCheckedRows();
		    	$.ajax({
		    		type:'post',
					url: '<%=request.getContextPath()%>/etlSchedule/startJobformation.action',
					contentType : "application/json;charset=UTF-8",
					dataType: 'json',
					data:JSON.stringify(rows),
		    		success:function(datas){
		    			for(var i in datas){
		    				grid.updateCell('jobStatus',datas[i].data.jobStatus,datas[i].data.__index);
		    				var rowobj = grid.getRowObj(datas[i].data.__index); 
		    				if (rowobj) $(rowobj).addClass("l-job-running");
		    			}
		    			
		    		}
		    	})
		    }
		    
		    function stopJobformation(obj){
		    	var rows = grid.getCheckedRows();
		    	$.ajax({
		    		type:'post',
					url: '<%=request.getContextPath()%>/etlSchedule/stopJobformation.action',
					contentType : "application/json;charset=UTF-8",
					dataType: 'json',
					data:JSON.stringify(rows),
		    		success:function(datas){
		    			//更新选择的行
		    			var selected = grid.getSelected();
		    			alert(selected.idJob);
		                if (!selected) { alert('请选择行'); return; }
		    		}
		    	})
		    }
		    
		    function refreshJobformationStatus(){ 
		    		
		    	//xuchengli 2015年9月22日0:22:01 获取所有行数据
		    	var rows = [];
		    	for (var rowid in grid.records)
	            {
	                var o = $.extend(true, {}, grid.records[rowid]);
	                rows.push(o);
	            }
		    	
		    	timer = setInterval(function(){
		    		$.ajax({
			    		type:'post',
						url: '<%=request.getContextPath()%>/etlSchedule/getJobformationStatus.action',
						contentType : "application/json;charset=UTF-8",
						dataType: 'json',
						data:JSON.stringify(rows),
			    		success:function(datas){
			    			for(var i in datas){
			    				grid.updateCell('jobStatus',datas[i].data.jobStatus,datas[i].data.__index);
			    				var rowobj = grid.getRowObj(datas[i].data.__index); 
			    				if (rowobj) $(rowobj).addClass("l-job-running");
			    			}
			    		}
			    	})
	    	    }, 2000);  
		    }
		    
		    function open_dialog(w, h, title, url){
		    	if (w == null || w == '') {
		    		w=800;
		    	};
		    	if (h == null || h == '') {
		    		h=($(window).height() - 50);
		    	};
		    	if (title == null || title == '') {
		    		title=false;
		    	};
		    	if (url == null || url == '') {
		    		url="404.html";
		    	};
		    	$.layer({
		        	type: 2,
		        	shadeClose: true,
		        	title: title,
		    		maxmin:false,
		    		shadeClose: true,
		        	closeBtn: [0, true],
		        	shade: [0.8, '#000'],
		        	border: [0],
		        	offset: ['20px',''],
		        	area: [w+'px', h +'px'],
		        	iframe: {src: url}
		    	});
		    }
	    </script>
	    <style>
	    	.l-job-running{
	    		background: #AFE711;
	    	}
	    </style>
	</head>
	
	<body style="overflow-x:hidden; padding:2px;">
		<div class="l-loading" style="display:block" id="pageloading"></div>
		 
		 <div class="l-clear"></div>
		
		    <div id="maingrid"></div>
		   
		  <div style="display:none;">
		  
		</div>
		 
	</body>
</html>