<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Kettle运行日志查询</title>
	    <script src="<%=request.getContextPath()%>/static/weblib/jquery/jquery-1.10.2.min.js" type="text/javascript"></script>
	    <script type="text/javascript">
	    	$(function (){
	    		var textarea= document.getElementById("textarea"); 
		    	//textarea.style.height=textarea.scrollHeight + "px"; 
		    	textarea.style.height=textarea.value.length/2 + "px"; 
            });
	    	
	    </script>
	    <style type="text/css">
.textarea{
          font-size: 12px;
          overflow:hidden;
          background-color: #fff;
          color: #000;
          padding-right:5px;
          padding-left:5px;
          font-family: courier;
          width:100%;
          letter-spacing:0;
          line-height:12px;
          border-style:1px #ccc solid;
          overflow-y:visible;
        }
</style>
	</head>
	
	<body >
		<c:choose>
	        <c:when test="${not empty jobLogText}">
				<textarea id="textarea" class="textarea">
					${jobLogText.logField}
				</textarea>
			</c:when>
			<c:otherwise>
		        	${NO_DATA}
		    </c:otherwise>
		</c:choose>
	</body>
</html>